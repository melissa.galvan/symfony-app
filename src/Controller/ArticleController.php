<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/all-articles", name="all-articles")
     */
    public function allArticles(ArticleRepository $articleRepo): Response
    {
        $articles = $articleRepo->findAll();

        return $this->render('article/all-articles.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/add-article", name="add-article")
     */
    public function addArticle(Request $request): Response
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $article = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
        }

        return $this->renderForm('article/new.html.twig', [
            'form' => $form
        ]);
    }


    /**
     * @Route("/one-article/{id}", name="one-article")
     */
    public function oneArticles(Article $article, ArticleRepository $articleRepo): Response
    {
        $articleRepo->find($article);

        return $this->render('article/one-article.html.twig', [
            'article' => $article,
        ]);
    }
}
